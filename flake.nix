{
  description = "Personal flake for Desktop, Laptop and Home-Server";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-colors = {
      url = "github:misterio77/nix-colors";
    };

    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ags = {
      url = "github:Aylur/ags";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    spicetify-nix = {
      url = "github:Gerg-L/spicetify-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    anyrun = {
      url = "github:Kirottu/anyrun";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    walker = {
      url = "github:abenz1267/walker";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    catppuccin = {
      url = "github:catppuccin/nix";
    };

    catppuccin-vsc.url = "https://flakehub.com/f/catppuccin/vscode/*.tar.gz";
  };
  outputs =
    { nixpkgs, home-manager, ... }@inputs:
    let
      system = "x86_64-linux";
      user = "atman";
      pkgs = nixpkgs.legacyPackages.${system};
      lib = nixpkgs.lib;
    in
    {
      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixfmt-rfc-style;

      homeConfigurations = {

        desktop = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          extraSpecialArgs = {
            inherit inputs;
            inherit user;
            host = {
              hostName = "desktop";
              monitor1 = "DP-2";
              monitor2 = "HDMI-A-1";
              monitor3 = "HDMI-A-2";
            };
          };
          modules = [
            inputs.catppuccin.homeManagerModules.catppuccin
            ./hosts/home.nix
            ./hosts/desktop/home.nix
          ];
        };

        laptop = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          extraSpecialArgs = {
            inherit inputs;
            inherit user;
            host = {
              hostName = "laptop";
              monitor1 = "eDP-1";
              monitor2 = "HDMI-A-1";
              monitor3 = "";
            };
          };
          modules = [
            inputs.catppuccin.homeManagerModules.catppuccin
            ./hosts/home.nix
            ./hosts/laptop/home.nix
          ];
        };

        server = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          extraSpecialArgs = {
            inherit inputs;
            inherit user;
          };
          modules = [
            ./hosts/home.nix
          ];
        };

        work-laptop = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          extraSpecialArgs = {
            inherit inputs;
            inherit user;
            host = {
              hostName = "work-laptop";
              monitor1 = "eDP-1";
              monitor2 = "HDMI-A-1";
              monitor3 = "";
            };
          };
          modules = [
            inputs.catppuccin.homeManagerModules.catppuccin
            ./hosts/home.nix
            ./hosts/work-laptop/home.nix
          ];
        };
      };

      nixosConfigurations = {

        desktop = lib.nixosSystem {
          specialArgs = {
            inherit inputs;
            inherit system;
          };
          modules = [
            ./hosts/configuration.nix
            ./hosts/desktop
          ];
        };

        laptop = lib.nixosSystem {
          specialArgs = {
            inherit inputs;
            inherit system;
          };
          modules = [
            ./hosts/configuration.nix
            ./hosts/laptop
          ];
        };

        server = lib.nixosSystem {
          specialArgs = {
            inherit inputs;
            inherit system;
          };
          modules = [
            ./hosts/configuration.nix
            ./hosts/server
            inputs.agenix.nixosModules.default
          ];
        };

        work-laptop = lib.nixosSystem {
          specialArgs = {
            inherit inputs;
            inherit system;
          };
          modules = [
            ./hosts/configuration.nix
            ./hosts/work-laptop
          ];
        };
      };
    };
}
