# NixOS Config

## Note

This is my personal NixOS config, it includes configuration for my desktop, laptop and my server.
I can advice you to only use this config for inspiration, as it is very specific to me and will properly not work for you.

## Example Installation

### Switch to root user

```bash
sudo su
```

### Change keyboard layout

```bash
loadkeys de
```

### Partition the disk

```bash
fdisk /dev/diskX
```

- g (gpt disk label)
- n
- 1 (partition number [1/128])
- 2048 first sector
- +512M last sector (boot sector size)
- t
- 1 (EFI System)
- n
- 2
- default (fill up partition)
- default (fill up partition)
- w (write)

### Check disks

```bash
lsblk
```

### Encrypt primary partition (optional)

```bash
cryptsetup luksFormat /dev/sdX2
cryptsetup luksOpen /dev/sdX2 crypted
```

### Format the partitions

```bash
mkfs.fat -F 32 /dev/sdX1
fatlabel /dev/sdX1 NIXBOOT
# for encryption
mkfs.btrfs /dev/mapper/crypted -L NIXROOT
# otherwise
mkfs.btrfs /dev/sdX2 -L NIXROOT
```

### Mount the file systems

```bash
mount /dev/disk/by-label/NIXROOT /mnt
mkdir -p /mnt/boot
mount /dev/disk/by-label/NIXBOOT /mnt/boot
```

### Generate config

```bash
nixos-generate-config --root /mnt
nix-env -iA nixos.git
git clone https://gitlab.com/p4rcev4l/nixos-config /mnt/etc/nixos/nixos-config
cp /mnt/etc/nixos/hardware-configuration.nix /mnt/etc/nixos/nixos-config/hosts/<host>/.
```

### Install

```bash
nixos-install --flake /mnt/etc/nixos/nixos-config/#<host>
```

### Change directory structure

```bash
mv /etc/nixos/nixos-config ~
chown -R atman:users ~/nixos-config
rm /etc/nixos/configuration.nix
```

### Setup up user

```bash
passwd atman
su - atman
```

### Switch to home-manager generation

```bash
home-manager switch --flake ~/nixos-config/#<host>
```

# :heart: Thanks to :heart:

- Matthias Benaets for his [nixos-config](https://github.com/MatthiasBenaets/nixos-config)
- [Vimjoyer](https://www.youtube.com/@vimjoyer) for the best NixOS video tutorials
