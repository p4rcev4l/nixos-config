################
##  hypridle  ##
################

{
  pkgs,
  ...
}:

{
  services.hypridle = {
    enable = true;

    settings = {
      general = {
        lock_cmd = "pidof ${pkgs.hyprlock}/bin/hyprlock || ${pkgs.hyprlock}/bin/hyprlock";
        before_sleep_cmd = "loginctl lock-session";
        after_sleep_cmd = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
      };

      listener = [
        {
          timeout = 300;
          on-timeout = "changeBrightness -s set 0%";
          on-resume = "changeBrightness -r";
        }
        {
          timeout = 600; # 10min
          on-timeout = "loginctl lock-session";
        }
        {
          timeout = 630; # 10.5min
          on-timeout = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off";
          on-resume = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
        }
        {
          timeout = 3600; # 60min
          on-timeout = "systemctl suspend";
        }
      ];
    };
  };
}
