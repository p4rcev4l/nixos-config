##############
##  neovim  ##
##############

{
  pkgs,
  ...
}:

{
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;

    plugins = with pkgs.vimPlugins; [
      # Syntax
      vim-nix
      vim-markdown

      # Quality of life
      vim-lastplace # Opens document where you left it
      auto-pairs # Print double quotes/brackets/etc
      vim-gitgutter # See uncommitted changes of file :GitGutterEnable

      # File Tree
      nerdtree # File Manager - set in extraConfig to F6
      nerdtree-git-plugin # git status
      vim-devicons # Icon Support

      # Colors
      catppuccin-nvim
    ];

    extraConfig = ''
      syntax enable                             " Syntax highlighting
      colorscheme catppuccin-mocha              " Color scheme text

      highlight Comment cterm=italic gui=italic " Comments become italic
      hi Normal guibg=NONE ctermbg=NONE         " Remove background, better for personal theme

      set number                                " Set numbers

      nmap <F6> :NERDTreeToggle<CR>             " F6 opens NERDTree

      let NERDTreeShowHidden=1                  " show hidden files in NERDTree
      let NERDTreeShowBookmarks=1               " open bookmarks panel on startup
    '';
  };
}
