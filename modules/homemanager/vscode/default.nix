##############
##  vscode  ##
##############

{
  pkgs,
  inputs,
  host,
  ...
}:

{
  nixpkgs.overlays = [ inputs.catppuccin-vsc.overlays.default ];

  programs.vscode = {
    enable = true;
    package = pkgs.vscode-fhs;
    profiles.default = {
      enableExtensionUpdateCheck = false;
      enableUpdateCheck = false;
      userSettings = {
        "editor.fontFamily" = "FiraCode Nerd Font Mono";
        "editor.fontSize" = 14;
        "terminal.integrated.fontSize" = 14;
        "terminal.integrated.scrollback" = 10000;
        "editor.mouseWheelZoom" = true;
        "window.zoomLevel" = 0;

        "files.autoSave" = "onFocusChange";
        "window.menuBarVisibility" = "toggle";
        "explorer.confirmDelete" = false;
        "files.trimTrailingWhitespace" = true;
        "redhat.telemetry.enabled" = false;

        "git.autofetch" = true;
        "git.mergeEditor" = true;
        "git.confirmSync" = false;
        "diffEditor.ignoreTrimWhitespace" = true;
        "git.suggestSmartCommit" = false;

        "java.jdt.ls.java.home" = "/etc/profiles/per-user/atman/lib/openjdk";

        "nix.enableLanguageServer" = true;
        "nix.serverPath" = "nixd";
        "nix.serverSettings" = {
          "nixd" = {
            "formatting" = {
              "command" = [ "nixfmt" ];
            };
            "options" = {
              "nixos" = {
                "expr" =
                  "(builtins.getFlake \"/home/atman/nixos-config\").nixosConfigurations.${host.hostName}.options";
              };
              "home-manager" = {
                "expr" =
                  "(builtins.getFlake \"/home/atman/nixos-config\").homeConfigurations.${host.hostName}.options";
              };
            };
          };
        };
        "[nix]" = {
          "editor.defaultFormatter" = "jnoortheen.nix-ide";
        };

        # Catppuccin Mocha
        "workbench.colorTheme" = "Catppuccin Mocha";

        # Material Icons
        "workbench.iconTheme" = "material-icon-theme";
        "workbench.productIconTheme" = "material-product-icons";

        # GitHub Copilot
        "github.copilot.editor.enableAutoCompletions" = true;

        # Prettier
        "editor.formatOnSave" = true;
        "editor.defaultFormatter" = "esbenp.prettier-vscode";

      };
      extensions = with pkgs.vscode-extensions; [
        # java extensions
        redhat.java
        vscjava.vscode-java-test
        vscjava.vscode-java-debug
        vscjava.vscode-java-dependency
        vscjava.vscode-maven

        # python extensions
        ms-python.python
        ms-python.vscode-pylance

        # nix extensions
        jnoortheen.nix-ide

        # C/C++ extensions
        ms-vscode.cpptools

        # Rust extensions
        rust-lang.rust-analyzer

        # themes
        (pkgs.catppuccin-vsc.override {
          accent = "pink";
          workbenchMode = "flat";
        })
        pkief.material-icon-theme
        pkief.material-product-icons

        # pdf
        tomoki1207.pdf

        # ssh
        ms-vscode-remote.remote-ssh

        # git
        eamodio.gitlens

        # docker
        ms-azuretools.vscode-docker

        # Copilot
        github.copilot
        github.copilot-chat

        # Spell checker
        streetsidesoftware.code-spell-checker

        # Markdown
        shd101wyy.markdown-preview-enhanced

        # Prettier
        esbenp.prettier-vscode

        # JSON
        zainchen.json
      ];
    };
  };
}
