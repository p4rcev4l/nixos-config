##############
##  walker  ##
##############

{
  pkgs,
  inputs,
  ...
}:

{
  # add the home manager module
  imports = [ inputs.walker.homeManagerModules.default ];

  programs.walker = {
    enable = false;
    # runAsService = true;

    # All options from the config.json can be used here.
    config = {
      theme = "catppuccin";
      force_keyboard_focus = true;
      search.placeholder = "Example";
      list = {
        height = 100;
      };
      #   websearch = {
      #     prefix = "?";
      #     engines = [ "duckduckgo" ];
      #   };
      #   switcher.prefix = "/";
    };
  };
}
