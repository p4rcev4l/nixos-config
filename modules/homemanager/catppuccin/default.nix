##################
##  catppuccin  ##
##################

{
  ...
}:

{
  catppuccin = {
    flavor = "mocha";
    accent = "pink";
  };
}
