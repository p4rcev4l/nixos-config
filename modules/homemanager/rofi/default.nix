############
##  rofi  ##
############

{
  pkgs,
  config,
  ...
}:
let
  inherit (config.lib.formats.rasi) mkLiteral;
in
{
  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    terminal = "${pkgs.alacritty}/bin/alacritty";
    location = "center";
    font = "FiraCode Nerd Font Mono 14";

    extraConfig = {
      display-drun = "❯";
      display-calc = "❯";
      combi-hide-mode-prefix = true;
      modes = "drun,calc";
      show-icons = true;
      icon-theme = "Papirus-Dark";
    };
    theme = with config.colorScheme.palette; {
      "*" = {
        border = 0;
        margin = 0;
        padding = 0;
        spacing = 0;
      };

      "window" = {
        width = mkLiteral "30%";
        background-color = mkLiteral "#${base}";
        border-radius = 5;
        border = 2;
        border-color = mkLiteral "#${pink}";
      };

      "element" = {
        padding = mkLiteral "8 12";
        background-color = mkLiteral "transparent";
        text-color = mkLiteral "#${text}";
      };

      "element selected" = {
        background-color = mkLiteral "#${pink}";
        text-color = mkLiteral "#${crust}";
      };

      "element-text" = {
        background-color = mkLiteral "transparent";
        text-color = mkLiteral "inherit";
        vertical-align = "0.5";
      };

      "element-icon" = {
        size = 18;
        padding = mkLiteral "0 10 0 0";
        background-color = mkLiteral "transparent";
      };

      "entry" = {
        padding = 12;
        background-color = mkLiteral "#${base}";
        text-color = mkLiteral "#${text}";
      };

      "inputbar" = {
        children = map mkLiteral [
          "prompt"
          "entry"
        ];
        background-color = mkLiteral "#${base}";
      };

      "listview " = {
        background-color = mkLiteral "#${base}";
        cycle = false;
        columns = 1;
        lines = 7;
      };

      "mainbox" = {
        children = map mkLiteral [
          "inputbar"
          "listview"
          "message"
        ];
        background-color = mkLiteral "#${base}";
      };

      "prompt" = {
        enabled = true;
        padding = mkLiteral "12 0 0 12";
        background-color = mkLiteral "#${base}";
        text-color = mkLiteral "#${text}";
      };

      "message" = {
        background-color = mkLiteral "#${base}";
        text-color = mkLiteral "#${text}";
      };

      "textbox" = {
        background-color = mkLiteral "#${base}";
        text-color = mkLiteral "#${text}";
      };
    };
  };
}
