################
##  mangohud  ##
################

{
  ...
}:

{
  programs.mangohud = {
    enable = true;
    settings = {
      gpu_temp = true;
      cpu_temp = true;
      fps_metrics = "avg,0.01";
      frame_timing = 0;
      output_folder = "~/Documents/mangohud/";
    };
  };
}
