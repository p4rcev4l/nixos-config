#############
##  kitty  ##
#############

{
  ...
}:

{
  catppuccin.kitty.enable = true;
  programs.kitty = {
    enable = true;
    font = {
      name = "FiraCode Nerd Font Mono";
      size = 12;
    };
    shellIntegration.enableZshIntegration = true;
    shellIntegration.enableFishIntegration = true;
    settings = {
      background_opacity = "1";
      confirm_os_window_close = 0;
    };
  };
}
