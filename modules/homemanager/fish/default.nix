############
##  fish  ##
############

{
  ...
}:

{
  imports = [
    ../starship
    ../fzf
  ];

  catppuccin.fish.enable = true;
  programs.fish = {
    enable = true;

    functions = {
      fish_greeting = "";
    };

    shellInit = ''
      bind \b backward-kill-word
    '';
  };
}
