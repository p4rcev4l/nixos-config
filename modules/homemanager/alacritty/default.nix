#################
##  alacritty  ##
#################

{
  ...
}:

{
  programs.alacritty = {
    enable = true;
    settings = {
      font = {
        normal.family = "FiraCode Nerd Font Mono";
        size = 12;
      };
    };
  };
}
