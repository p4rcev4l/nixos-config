#################
##  hyprpaper  ##
#################

{
  ...
}:

{
  services.hyprpaper = {
    enable = true;

    settings = {
      splash = "off";

      preload = [
        "~/nixos-config/assets/images/wallpapers/Kurzgesagt - Black Hole.png"
      ];

      wallpaper = [
        ",~/nixos-config/assets/images/wallpapers/Kurzgesagt - Black Hole.png"
      ];
    };
  };
}
