################
##  hyprlock  ##
################

{
  config,
  ...
}:

{
  catppuccin.hyprlock.enable = true;
  programs.hyprlock = {
    enable = true;

    settings = with config.colorScheme.palette; {
      general = [
        {
          disable_loading_bar = true;
          grace = 5;
          hide_cursor = true;
          no_fade_in = true;
          no_fade_out = true;
        }
      ];

      background = [
        {
          color = "rgb(${base})";
          path = "~/nixos-config/assets/images/wallpapers/catppuccin-mocha-pink.jpg";
        }
      ];

      input-field = [
        {
          check_color = "rgb(${pink})";
          dots_center = true;
          dots_size = 0.3;
          dots_spacing = 0.5;
          fade_on_empty = false;
          fail_color = "rgb(${red})";
          fail_text = "<i>$FAIL <b>($ATTEMPTS)</b></i>";
          font_color = "rgb(${text})";
          font_family = "FiraCode Nerd Font Mono";
          hide_input = false;
          inner_color = "rgb(${base})";
          outline_thickness = 4;
          outer_color = "rgb(${pink})";
          placeholder_text = "<span foreground=\"##$textAlpha\"><i>Logged in as </i><span foreground=\"##$accentAlpha\">$USER</span></span>";
          position = "0, -47";
          size = "300, 50";
          halign = "center";
          valign = "center";
        }
      ];
      image = [
        {
          border_color = "rgb(${pink})";
          halign = "center";
          path = "~/nixos-config/assets/images/profile/bot.png";
          position = "0, 65";
          size = 100;
          valign = "center";
        }
      ];
    };
  };
}
