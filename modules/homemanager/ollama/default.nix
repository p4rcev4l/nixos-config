##############
##  neovim  ##
##############

{
  pkgs,
  ...
}:

{
  services.ollama = {
    enable = false;
    acceleration = false;
    host = "0.0.0.0";
    port = 11434;
  };
}
