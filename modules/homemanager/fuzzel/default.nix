##############
##  fuzzel  ##
##############

{
  pkgs,
  config,
  ...
}:
{
  programs.fuzzel = {
    enable = false;
    settings = {
      main = {
        terminal = "${pkgs.kitty}/bin/kitty";
        icon-theme = "Papirus-Dark";
        font = "FiraCode Nerd Font Mono:weight=regular:size=14";
        dpi-aware = "yes";
        prompt = "' : '";
        lines = 7;
        width = 50;
        horizontal-pad = 0;
        vertical-pad = 0;
        inner-pad = 0;
        line-height = 35;
        fields = "name,generic,comment,categories,filename,keywords";
        letter-spacing = 2;
      };

      colors = with config.colorScheme.palette; {
        background = "${base}ff";
        text = "${text}ff";
        selection = "${pink}ff";
        selection-text = "${crust}ff";
        border = "${pink}ff";
      };

      border = {
        radius = 5;
        width = 2;
      };

      dmenu = {
        exit-immediately-if-empty = "yes";
      };

    };
  };
}
