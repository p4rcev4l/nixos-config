###############
##  firefox  ##
###############

{
  pkgs,
  inputs,
  ...
}:

{
  programs.firefox = {
    enable = true;
    package = pkgs.firefox-wayland;
    languagePacks = [
      "en-GB"
      "en-US"
      "de"
    ];
    profiles = {
      default = {
        id = 0;
        isDefault = true;
        search = {
          default = "DuckDuckGo";
          engines = {
            "Bing".metaData.hidden = true;
            "Google".metaData.hidden = true;
            "Nix Packages" = {
              urls = [
                {
                  template = "https://search.nixos.org/packages";
                  params = [
                    {
                      name = "type";
                      value = "packages";
                    }
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];

              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
              definedAliases = [ "@np" ];
            };

            "NixOS Wiki" = {
              urls = [ { template = "https://nixos.wiki/index.php?search={searchTerms}"; } ];
              iconUpdateURL = "https://nixos.wiki/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000; # every day
              definedAliases = [ "@nw" ];
            };
          };
          force = true;
        };
        extensions.packages = with inputs.firefox-addons.packages."x86_64-linux"; [
          bitwarden
          ublock-origin
          floccus
        ];
        settings = {
          "dom.security.https_only_mode_ever_enabled" = true;
          "dom.security.https_only_mode" = true;
          "datareporting.healthreport.uploadEnabled" = false;
          "privacy.trackingprotection.enabled" = true;
          "browser.contentblocking.category" = "strict";
        };
      };
    };
  };
}
