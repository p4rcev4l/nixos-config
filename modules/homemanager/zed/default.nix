###########
##  zed  ##
###########

{
  ...
}:

{
  catppuccin.zed.enable = true;
  programs.zed-editor = {
    enable = true;

    extensions = [
      "nix"
      "docker-compose"
    ];
    userSettings = {
      autosave = "on_focus_change";
      assistant = {
        default_model = {
          provider = "copilot_chat";
          model = "o3-mini";
        };
        version = "2";
      };
      enabled = true;
      buffer_font_size = 14;
      theme = {
        mode = "dark";
        dark = "Catppuccin Mocha (pink)";
      };
      ui_font_family = "FiraCode Nerd Font Mono";
      buffer_font_family = "FiraCode Nerd Font Mono";
      terminal = {
        cursor_shape = "bar";
        toolbar = {
          breadcrumbs = false;
        };
      };
    };
  };

  xdg.configFile."zed/themes/catppuccin-pink.json".source = ./catppuccin-pink.json;
}
