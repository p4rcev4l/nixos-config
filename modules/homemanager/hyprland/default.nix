################
##  hyprland  ##
################

{
  config,
  pkgs,
  host,
  ...
}:

let
  touchpad =
    with host;
    if hostName == "laptop" || hostName == "work-laptop" then
      ''
        touchpad {
          natural_scroll=true
          disable_while_typing = true
          tap-to-click=true
        }
      ''
    else
      "";

  gestures =
    with host;
    if hostName == "laptop" || hostName == "work-laptop" then
      ''
        gestures {
          workspace_swipe=true
          workspace_swipe_fingers=3
          workspace_swipe_distance=100
          workspace_swipe_cancel_ratio = 0.15
        }
      ''
    else
      "";

  monitors =
    with host;
    if hostName == "desktop" then
      ''
        monitor=${toString monitor1},2560x1440@144,0x0,1
        monitor=${toString monitor2},1920x1080@60,2560x180,1
        monitor=${toString monitor3},1920x1080@60,-1920x180,1

        # monitor=${toString monitor1},1920x1080@144,0x0,1
        # monitor=${toString monitor2},disable
        # monitor=${toString monitor3},disable
      ''
    else if hostName == "laptop" then
      ''
        monitor=${toString monitor1},1920x1080@60,0x1080,1
        monitor=${toString monitor2},1920x1080@60,0x0,1
      ''
    else if hostName == "work-laptop" then
      ''
        monitor=${toString monitor1},1920x1080@60,-1920x180,1
        monitor=${toString monitor2},2560x1440@60,0x0,1

        # monitor=${toString monitor1},1920x1080@60,0x1080,1
        # monitor=${toString monitor2},1920x1080@60,0x0,1
      ''
    else
      "";

  workspaces =
    with host;
    if hostName == "desktop" then
      ''
        workspace=1,monitor:${toString monitor1}, default:true
        workspace=2,monitor:${toString monitor2}, default:true
        workspace=3,monitor:${toString monitor3}, default:true

        workspace=1,monitor:${toString monitor1}
        workspace=2,monitor:${toString monitor2}
        workspace=3,monitor:${toString monitor3}
        workspace=4,monitor:${toString monitor1}
        workspace=5,monitor:${toString monitor2}
        workspace=6,monitor:${toString monitor3}
        workspace=7,monitor:${toString monitor1}
        workspace=8,monitor:${toString monitor2}
        workspace=9,monitor:${toString monitor3}
        workspace=10,monitor:${toString monitor1}
        workspace=11,monitor:${toString monitor2}
        workspace=12,monitor:${toString monitor3}
      ''
    else if hostName == "laptop" || hostName == "work-laptop" then
      ''
        workspace=1,monitor:${toString monitor1}, default:true
        workspace=2,monitor:${toString monitor2}, default:true

        workspace=1,monitor:${toString monitor1}
        workspace=2,monitor:${toString monitor2}
        workspace=3,monitor:${toString monitor1}
        workspace=4,monitor:${toString monitor2}
        workspace=5,monitor:${toString monitor1}
        workspace=6,monitor:${toString monitor2}
        workspace=7,monitor:${toString monitor1}
        workspace=8,monitor:${toString monitor2}
        workspace=9,monitor:${toString monitor1}
        workspace=10,monitor:${toString monitor2}
        workspace=11,monitor:${toString monitor1}
        workspace=12,monitor:${toString monitor2}
      ''
    else
      "";

in
{
  imports = [
    ../dunst
    ../rofi
    ../waybar

    ../hypridle
    ../hyprlock
    ../hyprpaper
  ];

  home.packages = with pkgs; [
    nautilus
    sushi
    gedit
    evince

    # home.packages = with kdePackages.pkgs; [
    # dolphin
    # dolphin-plugins
    # ark
    # kate
    # okular
    # gwenview
    # ];
  ];

  wayland.windowManager.hyprland = {
    enable = true;
    systemd.enable = false;
    extraConfig = with config.colorScheme.palette; ''
      ${workspaces}
      ${monitors}
      monitor=,preferred,auto,1

      #############################
      ### ENVIRONMENT VARIABLES ###
      #############################

      # See https://wiki.hyprland.org/Configuring/Environment-variables/

      # Toolkit Backend Variables (https://wiki.hyprland.org/Configuring/Environment-variables/#toolkit-backend-variables)
      env = GDK_BACKEND,wayland,x11,*
      env = QT_QPA_PLATFORM,wayland;xcb
      env = SDL_VIDEODRIVER,wayland
      env = CLUTTER_BACKEND,wayland

      # Qt Variables (https://wiki.hyprland.org/Configuring/Environment-variables/#qt-variables)
      env = QT_AUTO_SCREEN_SCALE_FACTOR,1
      env = QT_QPA_PLATFORM,wayland;xcb
      env = QT_WAYLAND_DISABLE_WINDOWDECORATION,1
      env = QT_QPA_PLATFORMTHEME,qt6ct # (modified qt6ct instead of qt5ct)

      # extra Qt Variables
      env = QT_STYLE_OVERRIDE,kvantum

      # Sets Cursor Theme
      exec-once = ${pkgs.hyprland}/bin/hyprctl setcursor Catppuccin-Mocha-Dark-Cursors 24

      # Autostart Pollkit
      exec-once = ${pkgs.kdePackages.polkit-kde-agent-1}/libexec/polkit-kde-authentication-agent-1 &

      # Autostart nextcloud-client
      exec-once = ${pkgs.nextcloud-client}/bin/nextcloud --background

      # Mediakey control
      bindl = ,XF86AudioRaiseVolume,exec,changeVolume 5%+
      bindl = ,XF86AudioLowerVolume,exec,changeVolume 5%-
      bindl = ,XF86AudioMute,exec,changeVolume mute
      bindl = ,XF86AudioPlay,exec,${pkgs.playerctl}/bin/playerctl play-pause
      bindl = ,XF86AudioPause,exec,${pkgs.playerctl}/bin/playerctl pause
      bindl = ,XF86AudioStop,exec,${pkgs.playerctl}/bin/playerctl stop
      bindl = ,XF86AudioNext,exec,${pkgs.playerctl}/bin/playerctl next
      bindl = ,XF86AudioPrev,exec,${pkgs.playerctl}/bin/playerctl previous

      # Brightness control
      bindl = ,XF86MonBrightnessUp,exec,changeBrightness set +1%
      bindl = ,XF86MonBrightnessDown,exec,changeBrightness set 1%-

      # Some default env vars.
      env = XCURSOR_SIZE,24

      # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
      input {
          kb_layout = de
          follow_mouse = 1
          sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
          ${touchpad}
      }

      ${gestures}

      device {
          name = logitech-mx-master-1
          accel_profile = flat
      }

      device {
          name = logitech-g305-1
          accel_profile = flat
      }

      device {
          name = la-1-wireless
          accel_profile = flat
      }

      general {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more
          gaps_in = 5
          gaps_out = 10
          border_size = 2
          col.active_border = rgba(${pink}ff)
          col.inactive_border = rgba(${surface0}aa)

          layout = dwindle
      }

      decoration {
          blur {
            enabled = false
          }
          rounding = 5
      }

      animations {
          enabled = yes

          # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more
          bezier = ease-out, 0, 0, .58, 1

          animation = windows, 1, 1, default
          animation = windowsOut, 1, 7, default
          animation = border, 1, 10, default
          animation = borderangle, 1, 8, default
          animation = fade, 1, 7, default
          animation = workspaces, 1, 5, default, fade
      }

      dwindle {
          # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
          pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
          preserve_split = yes # you probably want this
      }

      master {
          # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
          new_status = master
      }

      # See https://wiki.hyprland.org/Configuring/Keywords/ for more
      $mainMod = SUPER

      # Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
      bind = $mainMod, RETURN, exec, ${pkgs.kitty}/bin/kitty
      bind = $mainMod SHIFT, Q, killactive,
      bind = $mainMod, M, exit,
      bind = $mainMod, E, exec, ${pkgs.nautilus}/bin/nautilus
      bind = $mainMod, V, togglefloating,
      bind = $mainMod, F, fullscreen
      bind = $mainMod, D, exec, ${pkgs.rofi-wayland}/bin/rofi -show drun -modi drun
      bind = $mainMod, P, pseudo, # dwindle
      bind = $mainMod, S, togglesplit, # dwindle
      bind = CTRLALT, L, exec, ${pkgs.hyprlock}/bin/hyprlock --immediate

      # Move focus with mainMod + hjkl
      bind = $mainMod, h, movefocus, l
      bind = $mainMod, l, movefocus, r
      bind = $mainMod, k, movefocus, u
      bind = $mainMod, j, movefocus, d

      # move windows with mainMod
      bind = $mainMod SHIFT, H, movewindow, l
      bind = $mainMod SHIFT, L, movewindow, r
      bind = $mainMod SHIFT, K, movewindow, u
      bind = $mainMod SHIFT, J, movewindow, d

      # Switch workspaces with mainMod + [0-9]
      bind = $mainMod, 1, workspace, 1
      bind = $mainMod, 2, workspace, 2
      bind = $mainMod, 3, workspace, 3
      bind = $mainMod, 4, workspace, 4
      bind = $mainMod, 5, workspace, 5
      bind = $mainMod, 6, workspace, 6
      bind = $mainMod, 7, workspace, 7
      bind = $mainMod, 8, workspace, 8
      bind = $mainMod, 9, workspace, 9
      bind = $mainMod, 0, workspace, 10

      # Move active window to a workspace with mainMod + SHIFT + [0-9]
      bind = $mainMod SHIFT, 1, movetoworkspace, 1
      bind = $mainMod SHIFT, 2, movetoworkspace, 2
      bind = $mainMod SHIFT, 3, movetoworkspace, 3
      bind = $mainMod SHIFT, 4, movetoworkspace, 4
      bind = $mainMod SHIFT, 5, movetoworkspace, 5
      bind = $mainMod SHIFT, 6, movetoworkspace, 6
      bind = $mainMod SHIFT, 7, movetoworkspace, 7
      bind = $mainMod SHIFT, 8, movetoworkspace, 8
      bind = $mainMod SHIFT, 9, movetoworkspace, 9
      bind = $mainMod SHIFT, 0, movetoworkspace, 10


      # Move/resize windows with mainMod + LMB/RMB and dragging
      bindm = $mainMod, mouse:272, movewindow
      bindm = ALT, mouse:272, resizewindow

      # Screenshot
      bind = $mainMod SHIFT, S, exec, ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp)" - | ${pkgs.imagemagick}/bin/convert - -shave 1x1 PNG:- |  ${pkgs.satty}/bin/satty --early-exit --filename - --fullscreen --output-filename ~/Pictures/Screenshots/satty-$(date '+%Y%m%d-%H:%M:%S').png --copy-command ${pkgs.wl-clipboard}/bin/wl-copy

      # floats Firefox Picture-in-Picture windows
      windowrulev2 = float,class:firefox,title:Picture-in-Picture

      # floats Polkit KDE Authentication Agent
      windowrulev2 = float,class:org.kde.polkit-kde-authentication-agent-1,title:Authentication Required — PolicyKit1 KDE Agent

      # inbits idle when any window is open in fullscreen
      windowrulev2 = idleinhibit fullscreen,class:^.*$
    '';
  };
}
