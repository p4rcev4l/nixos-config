{
  pkgs,
  inputs,
  ...
}:
let
  spicePkgs = inputs.spicetify-nix.legacyPackages.${pkgs.system};
in
{
  imports = [ inputs.spicetify-nix.homeManagerModules.default ];

  programs.spicetify = {
    enable = true;

    # colorScheme = "custom";

    # customColorScheme = with config.colorScheme.palette; {
    #   text = "${pink}";
    #   subtext = "${text}";
    #   sidebar-text = "${text}";
    #   main = "${base}";
    #   sidebar = "${mantle}";
    #   player = "${mantle}";
    #   card = "${mantle}";
    #   shadow = "${crust}";
    #   selected-row = "${text}";
    #   button = "${pink}";
    #   button-active = "${pink}";
    #   button-disabled = "${text}";
    #   tab-active = "${pink}";
    #   misc = "${text}";
    # };

    theme = spicePkgs.themes.catppuccin;
    colorScheme = "mocha";

    enabledExtensions = with spicePkgs.extensions; [
      fullAppDisplay
      shuffle
      hidePodcasts
    ];
  };
}
