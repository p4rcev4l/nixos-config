##############
##  colors  ##
##############

{
  inputs,
  ...
}:

{
  imports = [
    inputs.nix-colors.homeManagerModules.default
  ];

  colorScheme = {
    name = "Catppuccin Mocha";
    author = "https://github.com/catppuccin/catppuccin";
    palette = {
      base = "1e1e2e"; # base00
      mantle = "181825"; # base01
      surface0 = "313244"; # base02
      surface1 = "45475a"; # base03
      surface2 = "585b70"; # base04
      text = "cdd6f4"; # base05
      rosewater = "f5e0dc"; # base06
      lavender = "b4befe"; # base07
      red = "f38ba8"; # base08
      peach = "fab387"; # base09
      yellow = "f9e2af"; # base0A
      green = "a6e3a1"; # base0B
      teal = "94e2d5"; # base0C
      blue = "89b4fa"; # base0D
      mauve = "cba6f7"; # base0E
      flamingo = "f2cdcd"; # base0F
      mantleAlt = "181825"; # base10
      crust = "11111b"; # base11
      maroon = "eba0ac"; # base12
      rosewaterAlt = "f5e0dc"; # base13
      greenAlt = "a6e3a1"; # base14
      sky = "89dceb"; # base15
      sapphire = "74c7ec"; # base16
      pink = "f5c2e7"; # base17
    };
  };

}
