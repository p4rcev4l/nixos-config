###########
##  gtk  ##
###########

{
  pkgs,
  ...
}:

let
  catppuccinAccent = "pink";
  catppuccinFlavor = "mocha";
in
{
  gtk = {
    enable = true;

    cursorTheme = {
      name = "catppuccin-mocha-dark-cursors";
      package = pkgs.catppuccin-cursors.mochaDark;
      size = 24;
    };

    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.catppuccin-papirus-folders.override {
        flavor = catppuccinFlavor;
        accent = catppuccinAccent;
      };
    };

    theme = {
      name = "catppuccin-mocha-pink-standard";
      package = pkgs.catppuccin-gtk.override {
        accents = [ catppuccinAccent ];
        size = "standard";
        variant = catppuccinFlavor;
      };
    };

    gtk3 = {
      extraConfig.gtk-application-prefer-dark-theme = true;
    };
  };

  home.pointerCursor = {
    name = "catppuccin-mocha-dark-cursors";
    package = pkgs.catppuccin-cursors.mochaDark;
    size = 24;
    gtk.enable = true;
  };
}
