#############
##  dunst  ##
#############

{
  pkgs,
  config,
  lib,
  ...
}:

let
  changeVolume = pkgs.writeShellScriptBin "changeVolume" ''
    msgTag="myvolume"

    if [[ "$@" == "mute" ]]; then
      wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
    else
      wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ "$@"
    fi


    volume_info=$(wpctl get-volume @DEFAULT_AUDIO_SINK@)

    if echo "$volume_info" | grep -q "MUTED"; then
      dunstify -a "changeVolume" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/audio-volume-muted-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"0" "Volume muted"
    else
      volume=$(echo "$volume_info" | tr -d '.' | tail -c 4 | sed 's/^0//')
      if [[ $volume -lt 33 ]]; then
        dunstify -a "changeVolume" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/audio-volume-low-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"$volume" "Volume: ''${volume}%"
      elif [[ $volume -lt 66 ]]; then
        dunstify -a "changeVolume" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/audio-volume-medium-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"$volume" "Volume: ''${volume}%"
      else
        dunstify -a "changeVolume" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/audio-volume-high-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"$volume" "Volume: ''${volume}%"
      fi
    fi
  '';

  changeBrightness = pkgs.writeShellScriptBin "changeBrightness" ''
    msgTag="mybrightness"

    ${pkgs.brightnessctl}/bin/brightnessctl "$@"

    brightness="$(${pkgs.brightnessctl}/bin/brightnessctl | grep 'Current brightness' | cut -d'(' -f2 | cut -d'%' -f1)"
    if [[ $brightness == 0 ]]; then
      dunstify -a "changeBrightness" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/display-brightness-off-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"0" "Brightness: ''${brightness}%"
    elif [[ $brightness -lt 33 ]]; then
      dunstify -a "changeBrightness" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/display-brightness-low-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"$brightness" "Brightness: ''${brightness}%"
    elif [[ $brightness -lt 66 ]]; then
      dunstify -a "changeBrightness" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/display-brightness-medium-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"$brightness" "Brightness: ''${brightness}%"
    else
      dunstify -a "changeBrightness" -u low -i ${pkgs.catppuccin-papirus-folders}/share/icons/Papirus-Dark/symbolic/status/display-brightness-high-symbolic.svg -h string:x-dunst-stack-tag:$msgTag -h int:value:"$brightness" "Brightness: ''${brightness}%"
    fi
  '';
in
{
  home.packages = [
    changeVolume
    changeBrightness
  ];

  services.dunst = {
    enable = true;
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.catppuccin-papirus-folders.override {
        flavor = "mocha";
        accent = "pink";
      };
    };
    settings = with config.colorScheme.palette; {
      global = {
        follow = "mouse";
        origin = "top-right";
        offset = "10x10";
        frame_color = lib.mkForce "#${pink}";
        frame_width = "2";
        separator_color = "frame";
        corner_radius = "5";
        font = "FiraCode Nerd Font 10";
        progress_bar_corner_radius = "3";
        highlight = lib.mkForce "#${pink}";
        background = lib.mkForce "#${base}";
        foreground = lib.mkForce "#${text}";
      };
      myvolume = {
        offset = "100x100";
      };
    };
  };
}
