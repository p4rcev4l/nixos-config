################
##  freetube  ##
################

{
  ...
}:

{
  programs.freetube = {
    enable = true;
    settings = {
      baseTheme = "catppuccinMocha";
      mainColor = "CatppuccinMochaPink";
      secColor = "CatppuccinMochaPink";
      defaultQuality = "1080";
      hideHeaderLogo = true;
    };
  };
}
