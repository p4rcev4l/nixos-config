###########
##  git  ##
###########

{
  pkgs,
  ...
}:

{
  programs.git = {
    enable = true;
    extraConfig = {
      pull.rebase = true;
      credential.helper = "${pkgs.git.override { withLibsecret = true; }}/bin/git-credential-libsecret";
    };
  };
}
