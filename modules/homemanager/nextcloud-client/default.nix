########################
##  nextcloud-client  ##
########################

{
  ...
}:

{
  services.nextcloud-client = {
    enable = false;
    startInBackground = true;
  };
}
