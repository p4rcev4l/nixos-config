###########
##  ZSH  ##
###########

{
  ...
}:

{
  imports = [
    ../starship
    ../fzf
  ];
  programs = {
    zsh = {
      enable = true;

      shellAliases = {
        "ll" = "ls -l";
        "la" = "ls -la";
      };

      enableCompletion = true;
      syntaxHighlighting.enable = true;
      autosuggestion.enable = true;

      initExtra = ''
        bindkey "^[[1;5C" forward-word
        bindkey "^[[1;5D" backward-word
        bindkey "^H" backward-kill-word
      '';
    };
  };
}
