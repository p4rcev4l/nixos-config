##############
##  waybar  ##
##############

{
  pkgs,
  config,
  host,
  ...
}:

{
  home.packages = with pkgs; [
    pavucontrol
    bluetuith
  ];

  programs.waybar = {
    enable = true;
    systemd.enable = true;
    settings = with host; [
      {
        layer = "top";
        position = "top";
        spacing = 10;
        output = [
          "${monitor1}"
          "${monitor2}"
          "${monitor3}"
        ];

        modules-left = [ "hyprland/workspaces" ];
        modules-center = [ "clock" ];

        modules-right =
          if hostName == "laptop" || hostName == "work-laptop" then
            [
              "tray"
              "bluetooth"
              "wireplumber"
              "network"
              "battery"
            ]
          else
            [
              "tray"
              "wireplumber"
              "network"
            ];

        "hyprland/workspaces" = {
          on-click = "activate";
          # format = "•";
        };

        "tray" = {
          spacing = 10;
        };

        "cpu" = {
          interval = 10;
          format = "{usage}% 󰘚  {avg_frequency}GHz󰐰";
        };

        "memory" = {
          interval = 10;
          format = "{percentage}% {used}GB";
        };

        "battery" = {
          interval = 60;

          format-discharging = "{capacity}% {icon}";
          format-charging = "{capacity}% {icon}󱐋";
          format-full = "{capacity}% {icon}󱐋";
          format-icons = [
            "󰁺"
            "󰁻"
            "󰁼"
            "󰁽"
            "󰁾"
            "󰁿"
            "󰂀"
            "󰂁"
            "󰂂"
            "󰁹"
          ];
          states = {
            warning = 30;
            critical = 15;
          };

          tooltip = true;
          tooltip-format = "{timeTo}\n{power}W";
        };

        "bluetooth" = {
          format-on = "󰂯";
          format-off = "󰂲";
          format-connected = "󰂱";
          format-connected-battery = "󰂱";
          tooltip-format-connected = "{device_enumerate}";
          tooltip-format-enumerate-connected = "{device_alias}";
          tooltip-format-enumerate-connected-battery = "{device_alias}\t{device_battery_percentage}%";

          on-click = "${pkgs.kitty}/bin/kitty ${pkgs.bluetuith}/bin/bluetuith";
        };

        "clock" = {
          format = "{:%a, %d. %b  %H:%M}";
        };

        "network" = {
          format-wifi = "{icon}";
          format-ethernet = "󰈀";
          format-disconnected = "󰤭";
          format-icons = [
            "󰤯"
            "󰤟"
            "󰤢"
            "󰤥"
            "󰤨"
          ];

          tooltip = true;
          tooltip-format-wifi = "{essid}\n{signalStrength}%\n{bandwidthUpBytes}\n{bandwidthDownBytes}";
          tooltip-format-ethernet = "{bandwidthUpBytes}\n{bandwidthDownBytes}";

          on-click = "${pkgs.kitty}/bin/kitty ${pkgs.networkmanager}/bin/nmtui";
        };

        "idle_inhibitor" = {
          format = "{icon}";
          format-icons = {
            activated = "";
            deactivated = "";
          };
        };

        "wireplumber" = {
          format = "{icon}";
          format-muted = "󰖁";
          format-icons = [
            "󰕿"
            "󰖀"
            "󰕾"
          ];

          on-click = "${pkgs.pavucontrol}/bin/pavucontrol";
        };
      }
    ];
    style = with config.colorScheme.palette; ''
      * {
        font-family: FiraCode Nerd Font;
        font-size: 15px;
      }

      window#waybar {
        border-radius: 5px;
        color: #${text};
        background: alpha(#${crust}, 0.65);
      }


      #tray {
        padding: 0px 20px 0px 0px;
      }

      #idle_inhibitor,
      #wireplumber,
      #cpu,
      #battery,
      #network,
      #bluetooth {
        padding: 0px 9px 0px 0px;
      }

      #workspaces button {
        padding: 0px 5px;
        border-radius: 5px;
        border: 2px solid transparent;
      }

      #workspaces button.active {
        border: 2px solid #${pink};
      }

      #battery.discharging.warning {
        color: #${yellow};
      }
      #battery.discharging.critical {
        color: #${red};
      }
    '';
  };
}
