##################
##  networking  ##
##################

{
  ...
}:

{
  networking.networkmanager.enable = true;
  users.users.atman.extraGroups = [ "networkmanager" ];
}
