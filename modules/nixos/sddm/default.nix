############
##  sddm  ##
############

{
  ...
}:

{
  services.xserver.xkb.layout = "de";
  services.displayManager.sddm.wayland.enable = true;
  services.displayManager.sddm.enable = true;
  services.displayManager.defaultSession = "hyprland-uwsm";
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "atman";

  security.pam.services.sddm.enableGnomeKeyring = true;
  security.pam.services.login.enableGnomeKeyring = true;
}
