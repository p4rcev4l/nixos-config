#################
##  nextcloud  ##
#################

{
  pkgs,
  config,
  ...
}:

{
  environment.etc."nextcloud-admin-pass".text = "admin"; # Should be changed after installation
  services.nextcloud = {
    enable = true;
    hostName = "tycho.tailnet-2ebc.ts.net";
    package = pkgs.nextcloud30;
    database.createLocally = true;
    maxUploadSize = "16G";

    appstoreEnable = true;
    autoUpdateApps.enable = true;
    extraApps = with config.services.nextcloud.package.packages.apps; {
      inherit
        contacts
        calendar
        tasks
        bookmarks
        notes
        ;
    };
    extraAppsEnable = true;
    settings = {
      trusted_domains = [ "tycho" ];
      trustedProxies = [ "127.0.0.1" ];
      overwriteprotocol = "https";

      maintantenance_window_start = 0;
      opcache.interned_strings_buffer = 16;
    };

    config = {
      dbtype = "pgsql";
      adminuser = "admin";
      adminpassFile = "/etc/nextcloud-admin-pass";
    };

    configureRedis = true;
  };
}
