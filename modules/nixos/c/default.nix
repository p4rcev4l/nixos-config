#########
##  C  ##
#########

{
  pkgs,
  ...
}:

{
  environment.systemPackages = with pkgs; [
    glib
    glibc
    rocmPackages.llvm.clang
    gnumake
    valgrind
  ];
}
