##############
##  ClamAV  ##
##############

{
  ...
}:

{
  services.clamav = {
    scanner = {
      enable = false;
      scanDirectories = [
        "/home"
        "/var/lib"
        "/tmp"
        "/etc"
        "/var/tmp"
      ];
    };
    daemon.enable = true;
    updater.enable = true;
    fangfrisch.enable = true;
  };
}
