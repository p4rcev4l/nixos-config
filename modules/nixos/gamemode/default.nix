################
##  gamemode  ##
################

{
  ...
}:

{
  programs.gamemode = {
    enable = true;
  };
}
