################
##  hyprland  ##
################

{
  ...
}:

{
  # enables hyprland module
  programs.hyprland = {
    enable = true;
    withUWSM = true;
  };
}
