###########
##  adb  ##
###########

{
  ...
}:

{
  programs.adb.enable = true;
  users.users.atman.extraGroups = [ "adbusers" ];
}
