######################
##  virtualisation  ##
######################

{
  ...
}:

{
  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemu.ovmf.enable = true;

  users.users.atman.extraGroups = [ "libvirtd" ];
}
