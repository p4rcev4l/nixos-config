##############
##  restic  ##
##############

{
  pkgs,
  config,
  ...
}:

{
  environment.systemPackages = with pkgs; [
    restic
    rclone
  ];
  age.secrets.restic-protondrive.file = /home/atman/secrets/restic-protondrive.age;
  services.restic.backups = {

    nextcloud = {
      initialize = true;
      repository = "rclone:protondrive:restic/nextcloud";
      passwordFile = "${config.age.secrets.restic-protondrive.path}";
      paths = [ "/var/lib/nextcloud/" ];
      timerConfig = {
        OnCalendar = "daily";
        Persistent = true;
      };
      pruneOpts = [
        "--keep-daily 7"
        "--keep-weekly 4"
        "--keep-monthly 12"
        "--keep-yearly 7"
      ];

      extraBackupArgs = [ "--verbose" ];
      # backupPrepareCommand = ''
      #   ${pkgs.docker}/bin/docker exec -t immich_postgres pg_dumpall -c -U postgres > /tmp/immich-database.sql
      # '';
      # backupCleanupCommand = ''
      #   rm /tmp/immich-database.sql
      # '';
      rcloneConfigFile = "~/.config/rclone/rclone.conf";
      rcloneOptions = {
        protondrive-replace-existing-draft = "true";
      };
    };
  };
}
