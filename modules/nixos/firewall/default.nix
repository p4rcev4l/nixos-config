################
##  firewall  ##
################

{
  ...
}:

{
  networking.firewall = {
    enable = true;
    checkReversePath = "loose";
    allowedUDPPorts = [ 51820 ]; # wireguard
    allowedTCPPorts = [ 11434 ]; # ollama
  };
}
