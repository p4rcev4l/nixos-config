###########
##  nix  ##
###########

{
  pkgs,
  inputs,
  ...
}:

{
  environment.systemPackages = with pkgs; [
    nixd
    nixfmt-rfc-style
  ];

  nix.nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];
}
