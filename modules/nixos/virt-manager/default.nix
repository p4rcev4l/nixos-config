####################
##  virt-manager  ##
####################

{
  ...
}:

{
  programs.virt-manager.enable = true;
  programs.dconf.enable = true;
}
