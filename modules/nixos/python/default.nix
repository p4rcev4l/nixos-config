##############
##  python  ##
##############

{
  pkgs,
  ...
}:

{
  environment.systemPackages = [
    pkgs.python3
  ];
}
