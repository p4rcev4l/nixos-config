##################
##  borgbackup  ##
##################

{
  pkgs,
  config,
  ...
}:

{
  # Make sure:
  # 1. agenix is setup with corresponding passwords
  # 2. your private ssh key is in /root/.ssh
  # 3. the repo you want to use is added to /root/.ssh/known_hosts

  environment.systemPackages = with pkgs; [
    borgbackup
  ];
  age.secrets.borg-immich.file = /etc/nixos/secrets/borg-immich.age;
  age.secrets.borg-nextcloud.file = /etc/nixos/secrets/borg-nextcloud.age;
  age.secrets.borg-syncthing.file = /etc/nixos/secrets/borg-syncthing.age;

  services.borgbackup.jobs = {
    immich = {

      paths = [
        "/docker/appdata/immich/library"
        "/tmp/immich-database.sql"
      ];
      exclude = [
        "/docker/appdata/immich/library/thumbs/"
        "/docker/appdata/immich/library/encoded-video/"
      ];
      readWritePaths = [
        "/docker/appdata/immich/library"
        "/tmp"
      ];

      encryption = {
        mode = "repokey-blake2";
        passCommand = "cat ${config.age.secrets.borg-immich.path}";
      };
      repo = "ssh://z67e6vo8@z67e6vo8.repo.borgbase.com/./repo";
      compression = "auto,zstd";
      startAt = "*-*-* 06:00:00";
      prune.keep = {
        daily = 7;
        weekly = 4;
        monthly = 6;
      };
      preHook = ''
        ${pkgs.docker}/bin/docker exec -t immich_postgres pg_dumpall -c -U postgres > /tmp/immich-database.sql
      '';
      postHook = ''
        rm /tmp/immich-database.sql
      '';
    };

    nextcloud = {

      paths = [
        "/var/lib/nextcloud"
        "/tmp/nextcloud-sqlbkp.bak"
      ];
      exclude = [ "/var/lib/nextcloud/data/admin/files_trashbin" ];
      readWritePaths = [
        "/var/lib/nextcloud"
        "/tmp"
      ];

      encryption = {
        mode = "repokey-blake2";
        passCommand = "cat ${config.age.secrets.borg-nextcloud.path}";
      };
      repo = "ssh://w07zq1pb@w07zq1pb.repo.borgbase.com/./repo";
      compression = "auto,zstd";
      startAt = "*-*-* 06:00:00";
      prune.keep = {
        daily = 7;
        weekly = 4;
        monthly = 6;
      };
      preHook = ''
        ${pkgs.util-linux}/bin/runuser -u nextcloud -- ${pkgs.postgresql}/bin/pg_dump nextcloud -h /run/postgresql -U nextcloud -f /tmp/nextcloud-sqlbkp.bak
      '';
      postHook = ''
        rm /tmp/nextcloud-sqlbkp.bak
      '';
    };

    syncthing = {
      paths = [ "/docker/appdata/syncthing/Backups" ];
      readWritePaths = [ "/docker/appdata/syncthing/Backups" ];

      encryption = {
        mode = "repokey-blake2";
        passCommand = "cat ${config.age.secrets.borg-syncthing.path}";
      };

      repo = "ssh://ej7k7ex8@ej7k7ex8.repo.borgbase.com/./repo";
      compression = "auto,zstd";

      startAt = "*-*-* 06:00:00";
      prune.keep = {
        daily = 7;
        weekly = 4;
        monthly = 6;
      };
    };
  };
}
