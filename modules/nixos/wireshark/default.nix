#################
##  wireshark  ##
#################

{
  ...
}:

{
  programs.wireshark.enable = true;

  users.users.atman.extraGroups = [ "wireshark" ];
}
