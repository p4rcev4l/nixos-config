{
  ...
}:

{
  hardware.graphics.enable = true;

  services.xserver = {
    enable = true;
    videoDrivers = [ "amdgpu" ];
  };
  boot.initrd.kernelModules = [ "amdgpu" ];
}
