############
##  java  ##
############

{
  pkgs,
  ...
}:

{
  environment.systemPackages = with pkgs; [
    maven
  ];

  programs.java = {
    enable = true;
  };
}
