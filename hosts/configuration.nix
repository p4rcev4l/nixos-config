{
  pkgs,
  ...
}:

{
  imports = [
    ../modules/nixos/gnome-keyring
  ];
  # Configure bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.timeout = 0;

  # btrfs auto srub
  services.btrfs.autoScrub.enable = true;

  # Set system state version
  system.stateVersion = "23.05";

  # Set your time zone.
  time.timeZone = "Europe/Brussels";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LANGUAGE = "en_US.UTF-8";
    LC_ADDRESS = "de_DE.UTF-8";
    LC_IDENTIFICATION = "de_DE.UTF-8";
    LC_MEASUREMENT = "de_DE.UTF-8";
    LC_MONETARY = "de_DE.UTF-8";
    LC_NAME = "de_DE.UTF-8";
    LC_NUMERIC = "de_DE.UTF-8";
    LC_PAPER = "de_DE.UTF-8";
    LC_TELEPHONE = "de_DE.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Handle config generation
  nix.gc = {
    automatic = true;
    options = "--delete-older-than 28d";
    dates = "weekly";
  };
  nix.optimise.automatic = true;

  # Configure console keymap
  console.keyMap = "de";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.atman = {
    isNormalUser = true;
    description = "atman";
    shell = pkgs.fish;
  };
  programs.fish.enable = true;

  # font
  fonts.packages = with pkgs; [
    nerd-fonts.fira-code
  ];
  fonts.fontDir.enable = true;

  users.users.atman.extraGroups = [ "wheel" ];

  # List packages installed in system profile
  environment.systemPackages = with pkgs; [
    curl
    wget
    bottom
    home-manager
    git
  ];

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Enable SSH
  services.openssh.enable = true;
  boot.initrd.network.ssh.enable = true;
  programs.ssh.startAgent = true;

  # Flake support
  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
  ];

  # set @wheel as trusted users
  nix.settings.trusted-users = [
    "root"
    "@wheel"
  ];

  # USB
  services.udisks2.enable = true;
}
