{
  pkgs,
  lib,
  ...
}:

{
  imports = [
    ./hardware-configuration.nix

    # essential
    ../../modules/nixos/amdgpu
    ../../modules/nixos/pipewire
    ../../modules/nixos/networking

    # desktop
    ../../modules/nixos/hyprland
    ../../modules/nixos/sddm

    # development
    ../../modules/nixos/adb
    ../../modules/nixos/c
    ../../modules/nixos/java
    ../../modules/nixos/nix
    ../../modules/nixos/python
    ../../modules/nixos/rust
    ../../modules/nixos/wireshark

    # programs
    ../../modules/nixos/partition-manager
    ../../modules/nixos/seahorse

    # virtualisation
    ../../modules/nixos/virtualisation
    ../../modules/nixos/quickemu
    ../../modules/nixos/virt-manager

    # services
    ../../modules/nixos/clamav
    ../../modules/nixos/localsend

  ];

  # Tuxedo
  hardware.tuxedo-rs = {
    enable = true;
    tailor-gui.enable = true;
  };
  hardware.tuxedo-drivers.enable = true;

  # Logitech
  hardware.logitech.wireless = {
    enable = true;
    enableGraphical = true;
  };

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # polkit
  security.polkit.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Configures bluetooth behaviour
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };

  # Sets hostname
  networking.hostName = "Medina";

  boot.kernelParams = [ "amd-pstate=guided" ];

  powerManagement.enable = true;
  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Laptop suspends on lid-close when docked
  services.logind.lidSwitchDocked = "suspend";

  programs.ssh.askPassword = "${pkgs.seahorse}/libexec/seahorse/ssh-askpass";
}
