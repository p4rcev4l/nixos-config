{
  pkgs,
  ...
}:

{
  imports = [
    ../../modules/homemanager/kitty
    ../../modules/homemanager/alacritty

    ../../modules/homemanager/firefox
    ../../modules/homemanager/chromium

    ../../modules/homemanager/nextcloud-client

    ../../modules/homemanager/hyprland

    ../../modules/homemanager/vscode
    ../../modules/homemanager/neovim

    ../../modules/homemanager/gtk
    ../../modules/homemanager/qt
    ../../modules/homemanager/catppuccin

    ../../modules/homemanager/zed
  ];

  home.packages = with pkgs; [
    # passwords
    keepassxc
    bitwarden-desktop

    # office
    libreoffice
    hunspell
    hunspellDicts.de_DE
    hunspellDicts.en_US

    # communication
    thunderbird
    zoom-us
    mattermost-desktop
  ];
}
