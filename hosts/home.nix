{
  ...
}:

{
  imports = [
    ../modules/homemanager/fish
    ../modules/homemanager/zsh
    ../modules/homemanager/colors
    ../modules/homemanager/git
    ../modules/homemanager/gnome-keyring
  ];
  nixpkgs.config.allowUnfree = true;

  home.username = "atman";
  home.homeDirectory = "/home/atman";
  home.stateVersion = "23.05";

  programs.home-manager.enable = true;

  nix.gc.automatic = true;

  xdg.enable = true;
}
