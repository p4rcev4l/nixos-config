{
  pkgs,
  ...
}:

{
  imports = [
    ./hardware-configuration.nix

    # essential
    ../../modules/nixos/amdgpu
    ../../modules/nixos/pipewire
    ../../modules/nixos/networking

    # desktop
    ../../modules/nixos/hyprland
    ../../modules/nixos/sddm

    # development
    ../../modules/nixos/adb
    ../../modules/nixos/c
    ../../modules/nixos/java
    ../../modules/nixos/nix
    ../../modules/nixos/python
    ../../modules/nixos/rust
    ../../modules/nixos/wireshark

    # programs
    ../../modules/nixos/partition-manager
    ../../modules/nixos/seahorse
    ../../modules/nixos/steam
    ../../modules/nixos/gamemode

    # virtualisation
    ../../modules/nixos/virtualisation
    ../../modules/nixos/quickemu
    ../../modules/nixos/virt-manager

    # services
    ../../modules/nixos/tailscale
    ../../modules/nixos/clamav

    # network
    ../../modules/nixos/firewall
    ../../modules/nixos/localsend
  ];

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # polkit
  security.polkit.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Sets hostname
  networking.hostName = "Ganymede";

  boot.kernelParams = [ "amd-pstate=guided" ];

  # ROCm support
  environment.variables = {
    ROC_ENABLE_PRE_VEGA = "1";
  };

  # HIP support
  systemd.tmpfiles.rules = [
    "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}"
  ];

  # ProtonMail integration
  services.protonmail-bridge = {
    enable = true;
    path = with pkgs; [ gnome-keyring ];
  };
}
