{
  pkgs,
  inputs,
  ...
}:

{
  imports = [
    ./hardware-configuration.nix

    # essential
    ../../modules/nixos/networking

    # server
    ../../modules/nixos/nextcloud
    ../../modules/nixos/borgbackup

    # services
    ../../modules/nixos/tailscale
    ../../modules/nixos/clamav

  ];

  # Set hostname
  networking.hostName = "Tycho";

  # secrets
  environment.systemPackages = [
    inputs.agenix.packages."${pkgs.system}".default
  ];

  # Configure Docker
  virtualisation.docker = {
    enable = true;
    storageDriver = "btrfs";
  };
  users.users.atman.extraGroups = [ "docker" ];

  # needed for remote SSH
  programs.nix-ld.enable = true;
}
