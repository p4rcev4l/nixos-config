{
  lib,
  pkgs,
  ...
}:

{
  imports = [
    ./hardware-configuration.nix

    # essential
    ../../modules/nixos/amdgpu
    ../../modules/nixos/pipewire
    ../../modules/nixos/networking

    # desktop
    ../../modules/nixos/hyprland
    ../../modules/nixos/sddm

    # development
    ../../modules/nixos/adb
    ../../modules/nixos/c
    ../../modules/nixos/java
    ../../modules/nixos/nix
    ../../modules/nixos/python
    ../../modules/nixos/rust
    ../../modules/nixos/wireshark

    # programs
    ../../modules/nixos/partition-manager
    ../../modules/nixos/seahorse
    ../../modules/nixos/steam
    ../../modules/nixos/gamemode

    # virtualisation
    ../../modules/nixos/virtualisation
    ../../modules/nixos/quickemu
    ../../modules/nixos/virt-manager

    # services
    ../../modules/nixos/tailscale
    ../../modules/nixos/clamav

    # network
    ../../modules/nixos/firewall
    ../../modules/nixos/localsend
  ];

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # polkit
  security.polkit.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # Configures bluetooth behaviour
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = false;
  };

  # Sets hostname
  networking.hostName = "Ceres";

  boot.kernelParams = [ "amd-pstate=guided" ];

  powerManagement.enable = true;
  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Laptop suspends on lid-close when docked
  services.logind.lidSwitchDocked = "suspend";

  # ProtonMail integration
  services.protonmail-bridge = {
    enable = true;
    path = with pkgs; [ gnome-keyring ];
  };
}
