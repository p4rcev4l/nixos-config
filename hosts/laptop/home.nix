{
  pkgs,
  ...
}:

{
  imports = [
    ../../modules/homemanager/kitty
    ../../modules/homemanager/alacritty

    ../../modules/homemanager/chromium
    ../../modules/homemanager/firefox
    ../../modules/homemanager/freetube
    ../../modules/homemanager/spicetify

    ../../modules/homemanager/nextcloud-client
    ../../modules/homemanager/kdeconnect

    ../../modules/homemanager/hyprland

    ../../modules/homemanager/obs-studio
    ../../modules/homemanager/mangohud

    ../../modules/homemanager/vscode
    ../../modules/homemanager/neovim

    ../../modules/homemanager/gtk
    ../../modules/homemanager/qt
    ../../modules/homemanager/catppuccin

    ../../modules/homemanager/ollama
    ../../modules/homemanager/zed
  ];

  home.packages = with pkgs; [
    # design
    gimp
    krita

    # other
    ollama
    zotero
    bitwarden-desktop

    # office
    libreoffice
    hunspell
    hunspellDicts.de_DE
    hunspellDicts.en_US
    xournalpp

    # communication
    signal-desktop
    element-desktop
    thunderbird
    zoom-us

    # android
    android-tools
    payload-dumper-go
    android-file-transfer

    # gaming
    vesktop
    prismlauncher

    # media
    jellyfin-media-player
  ];
}
